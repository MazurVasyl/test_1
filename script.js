let N = 2;

if (N === 12 || (N >= 1 && N <= 2)) { console.log("winter") }

else if (N >= 3 && N <= 5) { console.log("spring") }

else if (N >= 6 && N <= 8) { console.log("summer") }

else if (N >= 9 && N <= 11) { console.log("autumn") }

else if (N < 1 || N > 12) { console.log("error") }
